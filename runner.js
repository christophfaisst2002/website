$(function(){
    registerClicks();
});

function changeID(ID) {
    console.log(ID);
    loadTrailer(ID);
}

function loadTrailer(ID) {
    $.ajax({
        url: "localhost/movies/" + ID,
        headers: {
            
        },
        method: "GET",
        success: function(data) {
            console.log(data.trailerurl);
            $('#trailerurl').attr('src', data);
        },
        error: function (error, response) {
            alert(error);
            alert(response);
        }
    })
}

function registerClicks () {
    $("#1").click(function(){
        changeID(1);
    });
    $("#2").click(function(){
        changeID(2);
    });
    $("#3").click(function(){
        changeID(3);
    });
    $("#4").click(function(){
        changeID(4);
    });
    $("#5").click(function(){
        changeID(5);
    });
    $("#6").click(function(){
        changeID(6);
    });
    $("#7").click(function(){
        changeID(7);
    });
    $("#8").click(function(){
        changeID(8);
    });
    $("#9").click(function(){
        changeID(9);
    });
    $("#10").click(function(){
        changeID(10);
    });
    $("#11").click(function(){
        changeID(11);
    });
    $("#12").click(function(){
        changeID(12);
    });
    $("#13").click(function(){
        changeID(13);
    });
    $("#14").click(function(){
        changeID(14);
    });
}
